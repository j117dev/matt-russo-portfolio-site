export enum Icon {
    Design = 'design',
    Code = 'code',
    Implementation = 'implementation',
    Communication = 'communication',
}